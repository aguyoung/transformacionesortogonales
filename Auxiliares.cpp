	#include <stdio.h>
	#include <stdlib.h>
	#include "math.h"
	#include "lapacke.h"
	#include "cblas.h"
        #include <string.h>

	void ImprimirResumen(FILE * fp, bool & bPrimerLog, float duracion,const char* metodo,unsigned long long m, unsigned long long n, unsigned long long hess, unsigned long long dimBloque, int cantHilos, float ProbablidadCero, bool bVerifica){
		if (bPrimerLog){
			(bPrimerLog)=false;
			printf("\nTransformaciones ortogonales de matices utilizando GPUs\n\n");
			printf("Duracion, algoritmo, m, n, hess, dimBloque, threadsBloque, p(0), verifica\n");
			fprintf(fp,"Duracion, algoritmo, m, n, hess, dimBloque, threadsBloque, p(0), verifica\n");
		}
		printf("%f, %s, %llu, %llu, %llu, %llu, %d, %f, %s\n",duracion,metodo,m,n,hess,dimBloque,cantHilos,ProbablidadCero,bVerifica?"SI":"NO");
		fprintf(fp,"%f, %s, %llu, %llu, %llu, %llu, %d, %f, %s\n",duracion,metodo,m,n,hess,dimBloque,cantHilos,ProbablidadCero,bVerifica?"SI":"NO");
	}

        void ImprimirMatriz(FILE * fp, float * M, unsigned long long m,unsigned long long n, unsigned long long l){
                printf("\n---------------\n");
                fprintf(fp,"\n---------------\n");
                for (int j = 0; j<m; j++){
                        for (int i = 0; i<n; i++){
                                printf("%f ", M[j+i*l]);
                                fprintf(fp,"%f ", M[j+i*l]);
                        }
                        printf("\n");
                        fprintf(fp,"\n");
                }
                printf("---------------\n");
                fprintf(fp,"---------------\n");
        }

        void ImprimirMatriz(FILE * fp, float * M, unsigned long long m,unsigned long long n){
        	ImprimirMatriz(fp, M, m, n, m);
	}

        void ImprimirMatrizPlan(FILE * fp, unsigned long long * M, unsigned long long n){
                printf("\n--------matriz aux planficacion-------\n");
                fprintf(fp,"\n--------matriz aux planficacion-------\n");
                for (int j = 0; j<n; j++){
                        for (int i = 0; i<=M[j]; i++){
                                printf("%d ", M[j+i*n]);
                                fprintf(fp,"%d ", M[j+i*n]);
                        }
                        printf("\n");
                        fprintf(fp,"\n");
		}
                printf("---------------\n");
                fprintf(fp,"---------------\n");
        }

        void ImprimirPlan1(FILE * fp, unsigned long long * plan){
                printf("\n---plan------------\n");
                fprintf(fp,"\n---plan------------\n");
                printf("%d ", plan[0]);
                fprintf(fp,"%d ", plan[0]);
                for (int i = 1; i<plan[0]*2+1; i+=2){
                        printf("%d,%d ", plan[i], plan[i+1]);
                        fprintf(fp,"%d,%d ", plan[i], plan[i+1]);
                }
                printf("\n---------------\n");
                fprintf(fp,"\n---------------\n");
        }

        void ImprimirPlan2(FILE * fp, unsigned long long * plan){
                printf("\n---plan------------\n");
                fprintf(fp,"\n---plan------------\n");
                printf("%d ", plan[0]);
                fprintf(fp,"%d ", plan[0]);
                for (int i = 1; i<plan[0]*3+1; i+=3){
                        printf("%d,%d,%d ", plan[i], plan[i+1],plan[i+2]);
                        fprintf(fp,"%d,%d,%d ", plan[i], plan[i+1],plan[i+2]);
                }
                printf("\n---------------\n");
                fprintf(fp,"\n---------------\n");
        }

        void ImprimirPivotes(FILE * fp, unsigned long long * M, unsigned long long n){
                printf("\n--------pivotes-------\n");
                fprintf(fp,"\n--------pivotes-------\n");
                for (int i = 0; i<n; i++){
                        printf("%d ", M[i]);
                        fprintf(fp,"%d ", M[i]);
                }
                printf("\n---------------\n");
                fprintf(fp,"\n---------------\n");
        }

        void CargarMatrizAleatoria(float* matriz, unsigned long long m, unsigned long long n,unsigned long long hess0, float ProbabilidadCero, float Semilla){
		//h0 determina el ancho de la banda de ceros de la zona la izquierda inferior de ceros (matriz de hessenberg). 0<= h0 < m-1
                srand(Semilla);
                for (unsigned long long y=0;y<m;y++){
                    for (unsigned long long x=0;x<n;x++){
			if (y<m-hess0+x){
                            matriz[y+x*m]=(1+rand()%100000)*0.0001;
                            if (matriz[y+x*m] < ProbabilidadCero) matriz[y+x*m]=0;
			}else{
                            matriz[y+x*m]=0;
			}
                    }
                }
        }

        bool EsDiagonalSuperior(FILE * fp,float * M, unsigned long long m, unsigned long long n, float Tolerancia){
                for (int y = 1; y<m; y++){
                        for (int x = 0; x<y && x<n; x++){
                                if (fabs(M[y+x*m])>Tolerancia){
                                        printf("\nError, la matriz no es triangular superior, falla en [%d,%d] valor[%f]\n\n",y,x,M[y+x*m]);
                                        fprintf(fp,"\nError, la matriz no es triangular superior\n\n");
                                        return false;
                                }
                        }
                }
                return true;
	}

        void MatrizIdentidad(float * M, unsigned long long m, unsigned long long n, float val){
		memset (M,0,sizeof(float)*m*n);
                for (int i = 0; i<m && i<n; i++){
                	M[i+i*m]=val;
                }
        }

        void TriangularInferior(float * U, unsigned long long m, unsigned long long n){
        //coloca unoss en la diagonal y ceros en la parte superior
        	unsigned long long min; 
                for (int y = 0; y<m; y++){
			U[y+y*m]=1;
			if (y>n){
				min=n;
			}else{
				min=y;
			}	
			memset (&U[y*m],0,sizeof(float)*min);
                }
        }

        void ProductoVectorEscalar(float * v, float e , unsigned long long n){
		cblas_sscal(n, e, v, 1);	
	}

        float* ProductoVector(float * M1, float * M2, unsigned long long n){
                float * R = (float*)malloc(sizeof(float)*n*n);
                for (int x = 0; x<n; x++){
                        for (int y = 0; y<n; y++){
                                R[y + x * n] = M1[y] * M2[x];
                        }
                }
                return R;
        }

        void TransponerMatriz(FILE * fp,float * M, unsigned long long m, unsigned long long n,bool bDebug){
                if (bDebug){
                       printf("Matriz antes de transponer\n");
                       fprintf(fp,"Matriz antes de transponer\n");
                       ImprimirMatriz(fp,M,m,n);
                }
                float * A = (float*)malloc(sizeof(float)*n*m);
                for (int y = 0; y<m; y++){
                       cblas_scopy(n,&M[y],m,&A[y*n],1);
                }
                memcpy(M, A,sizeof(float)*n*m);
                if (bDebug){
                       printf("Matriz despues de transponer\n");
                       fprintf(fp,"Matriz despues de transponer\n");
                       ImprimirMatriz(fp,M,n,m);
                }
                free(A);
        }

        void ProductoMatrices(CBLAS_TRANSPOSE M1tran, CBLAS_TRANSPOSE M2tran, float * M1, float * M2, float * R, unsigned long long Rm, unsigned long long Rn, unsigned long long k, float alpha, float beta){
		CBLAS_ORDER Order = CblasColMajor;
		float lda,ldb,ldc;
	        //condicion de producto de matrices n1=m2
		//CBLAS_TRANSPOSE {CblasNoTrans, CblasTrans}
		//R := alpha * op(M1) * op(M2) + beta * R
		//void cblas_sgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                //const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                //const int K, const float alpha, const float *A,
                //const int lda, const float *B, const int ldb,
                //const float beta, float *C, const int ldc);
		if (M1tran==CblasNoTrans){
			if (M2tran==CblasNoTrans){
                		lda=(int)Rm; ldb=(int)k;  ldc=(int)Rm;
			}else{
                		lda=(int)Rm; ldb=(int)Rn; ldc=(int)Rm;
			}
		}else{
			if (M2tran==CblasNoTrans){
                		lda=(int)k;  ldb=(int)k; ldc=(int)Rm;
			}else{
                		lda=(int)k;  ldb=(int)Rn; ldc=(int)Rm;
			}

		}
                cblas_sgemm(Order, M1tran, M2tran, (int)Rm, (int)Rn, (int)k, alpha, M1 ,lda, M2, ldb, beta, R, ldc);
        }

void ProductoMatrices(CBLAS_TRANSPOSE M1tran, CBLAS_TRANSPOSE M2tran, float * M1, float * M2, float * R, unsigned long long Rm, unsigned long long Rn, unsigned long long k){
                ProductoMatrices(M1tran, M2tran, M1, M2, R, Rm, Rn, k, 1, 0);
        }

        float NormaVector(float * v, unsigned long long n){
               return cblas_snrm2 (n,v,1);
        }

        float Signo(float n){
                return n<0?-1:1;
        }

        void ExtraerSubmatriz(FILE * fp, float* orig, float * subm, unsigned long long m, unsigned long long  n,unsigned long long  y,unsigned long long  x, unsigned long long dimy, unsigned long long dimx, unsigned long long diml, bool bDebug){
                LAPACKE_slacpy(LAPACK_COL_MAJOR,'A',dimy,dimx,&orig[y+x*m],m,subm,diml); 
                if (bDebug){
                        printf("Submatiz extraida coordenadas punto inicial (%d,%d), dim(%d,%d)\n",y,x,dimy,dimx);
                        fprintf(fp,"Submatiz extraida coordenadas punto inicial (%d,%d), dim(%d,%d)\n",y,x,dimy,dimx);
                        ImprimirMatriz(fp,subm,dimy,dimx);
                }
                
        }

        void ExtraerSubmatriz(FILE * fp, float* orig, float * subm, unsigned long long m, unsigned long long  n,unsigned long long  y,unsigned long long  x, unsigned long long dimy, unsigned long long dimx, bool bDebug){
        ExtraerSubmatriz(fp, orig, subm, m, n, y, x, dimy, dimx, dimy, bDebug);
	}

        void InsertarSubmatriz(FILE * fp, float* dest, float * subm, unsigned long long m, unsigned long long  n,unsigned long long  y,unsigned long long  x, unsigned long long dimy, unsigned long long dimx, bool bDebug){
                for (unsigned long long c = x; c < x+dimx; c++){
                        cblas_scopy(dimy,&subm[(c-x)*dimy],1,&dest[y+c*m],1);
                }
                if (bDebug){
                  printf("Inserta en matriz principal submatiz con punto inicial (%d,%d), dim(%d,%d)\n",y,x,dimy,dimx);
                  fprintf(fp,"Inserta en matriz principal submatiz con punto inicial (%d,%d), dim(%d,%d)\n",y,x,dimy,dimx);
                  ImprimirMatriz(fp,dest,m,n);
                }
        }



