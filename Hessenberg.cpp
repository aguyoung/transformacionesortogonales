
#include <stdio.h>
#include <string.h>
#include "lapacke.h"
#include "Auxiliares.h"

void HessenbergCPU(float * A, unsigned long long M, unsigned long long N, bool bDebug, FILE * fp){
	int matrix_order=LAPACK_COL_MAJOR;
	lapack_int lda=M;
	lapack_int n=N;
	lapack_int m=M;
	lapack_int ihi=N; 
	lapack_int ilo=1;
	if (M<N){
                fprintf(fp,"Error, el algoritmo de Hessenber solo admite matrices con M>=N\n");
                printf("Error, el algoritmo de Hessenber solo admite matrices con M>=N\n");
	}else{
		float* tau = (float*)malloc(sizeof(float)*(n-1));
	
		LAPACKE_sgehrd(matrix_order, n, ilo, ihi, A, lda, tau);

		for (int i =0; i<n; i++){
			memset (&A[i+1+i*m],0,sizeof(float)*(m-i-1));
		}
		if (bDebug){
                	fprintf(fp,"Matriz final de Hessenberg\n");
                	printf("Matriz final de Hessenberg\n");
        	        ImprimirMatriz(fp,A,m,n);
		}
	}
}


