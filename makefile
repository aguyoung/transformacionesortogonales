redhat = /usr/lib/gcc/x86_64-redhat-linux/4.8.2
lapack = ../lapack-3.4.2
cblas = ../CBLAS
flags = -llapacke -llapack -lgfortran -lblas_LINUX -lcublas -lm 

#export PATH=$(PATH):/usr/local/cuda/bin:/opt/cuda-6.0/bin/
PATH=/usr/lib64/qt-3.3/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/ens/home01/a/agustin.young/bin:/usr/local/cuda/bin:/opt/cuda-6.0/bin/

#export LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):/usr/local/cuda/lib64
LD_LIBRARY_PATH=/usr/local/cuda/lib64:/opt/cuda-6.0/lib64

all: clean ModulosCPU PrjCuda

ModulosCPU:
	nvcc -c -O3 -I$(lapack)/lapacke/include -I$(cblas)/include -L$(lapack) -L$(cblas)/lib -L../BLAS Auxiliares.cpp Givens.cpp Householder.cpp Hessenberg.cpp -arch=sm_20 -lrefblas $(flags)
	
PrjCuda: 
	nvcc -O3    -I$(lapack)/lapacke/include -I$(cblas)/include -L$(lapack) -L$(cblas)/lib -L../BLAS -L$(redhat) -o PrjCuda PrjCuda.cu Auxiliares.o Givens.o Householder.o Hessenberg.o $(cblas)/lib/cblas_LINUX.a $(lapack)/liblapacke.a --compiler-bindir "./" -arch=sm_20 $(flags)
 
clean: 
	rm -f *.o PrjCuda
	rm -f Salida*

