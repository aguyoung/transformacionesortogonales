#ifndef __AUXILIARES_H
#define __AUXILIARES_H
#include "cblas.h"

	void ImprimirResumen(FILE * fp, bool & bPrimerLog, float duracion,const char* metodo,unsigned long long m, unsigned long long n, unsigned long long hess, unsigned long long dimBloque, int cantHilos, float ProbablidadCero,bool bVerifica);
	void ImprimirMatriz(FILE * fp, float * M, unsigned long long m,unsigned long long n);
	void ImprimirMatriz(FILE * fp, float * M, unsigned long long m,unsigned long long n, unsigned long long l);
	void ImprimirMatrizPlan(FILE * fp, unsigned long long * M,unsigned long long n);
	void ImprimirPlan1(FILE * fp, unsigned long long * plan);
	void ImprimirPlan2(FILE * fp, unsigned long long * plan);
	void ImprimirPivotes(FILE * fp, unsigned long long * M, unsigned long long n);
        void CargarMatrizAleatoria(float * matriz, unsigned long long m, unsigned long long n,unsigned long long hess0, float ProbabilidadCero, float Semilla);
        bool EsDiagonalSuperior(FILE * fp, float * M, unsigned long long m, unsigned long long n, float Tolerancia);
	void MatrizIdentidad(float * M, unsigned long long m, unsigned long long n, float val);
	float* ProductoVector(float * M1, float * M2, unsigned long long n);
	void ProductoVectorEscalar(float * v, float e , unsigned long long n);
	void TransponerMatriz(FILE * fp, float * M, unsigned long long m, unsigned long long n, bool bDebug);
	void ProductoMatrices(CBLAS_TRANSPOSE M1tran, CBLAS_TRANSPOSE M2tran, float * M1, float * M2, float * R, unsigned long long Rm, unsigned long long Rn, unsigned long long k, float alpha, float beta);
	void ProductoMatrices(CBLAS_TRANSPOSE M1tran, CBLAS_TRANSPOSE M2tran, float * M1, float * M2, float * R, unsigned long long Rm, unsigned long long Rn, unsigned long long k);
        float NormaVector(float * v, unsigned long long n);
        void ExtraerSubmatriz(FILE * fp, float* orig, float * subm, unsigned long long m, unsigned long long  n,unsigned long long  y,unsigned long long  x, unsigned long long dimy, unsigned long long dimx, bool bDebug);
        void ExtraerSubmatriz(FILE * fp, float* orig, float * subm, unsigned long long m, unsigned long long  n,unsigned long long  y,unsigned long long  x, unsigned long long dimy, unsigned long long dimx, unsigned long long diml, bool bDebug);
        void InsertarSubmatriz(FILE * fp, float* dest, float * subm, unsigned long long m, unsigned long long  n,unsigned long long  x,unsigned long long  y, unsigned long long dimx, unsigned long long dimy, bool bDebug);
        void TriangularInferior(float * U, unsigned long long m, unsigned long long n);
	float Signo(float n);
#endif
