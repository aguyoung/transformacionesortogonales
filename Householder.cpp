
#include <stdio.h>
#include "lapacke.h"
#include "Auxiliares.h"
#include <string.h>

        void ExtraerSubmatricesEnterasU(FILE * fp, float * A11A21, float * U11U21, float * UT11UT21, unsigned long long mA11A21, unsigned long long nA11A21,  unsigned long long mA11, bool bDebug){
                unsigned long long yU11,xU11,mU11,nU11,yU21,xU21,mU21,nU21,aux,mUT11UT21,nUT11UT21;
                yU11=0;         xU11=0;     mU11=mA11;          nU11=nA11A21;
                yU21=yU11+mU11; xU21=xU11;  mU21=mA11A21-mU11;  nU21=nU11;
                                            mUT11UT21=nU11;     nUT11UT21=mU11+mU21;
                float * U11 = (float*)malloc(sizeof(float)*mU11*nU11);
                float * U21 = (float*)malloc(sizeof(float)*mU21*nU21);
                ExtraerSubmatriz(fp, A11A21, U11, mA11A21, nA11A21, yU11, xU11, mU11, nU11,false);
                ExtraerSubmatriz(fp, A11A21, U21, mA11A21, nA11A21, yU21, xU21, mU21, nU21,false);
                TriangularInferior(U11,mU11,nU11);
                InsertarSubmatriz(fp,U11U21, U11, mA11A21, nA11A21, 0    , 0, mU11, nU11,false);
                InsertarSubmatriz(fp,U11U21, U21, mA11A21, nA11A21, mU11 , 0, mU21, nU21,false);
                if (bDebug){
                       fprintf(fp,"U11(y,x,m,n)=(%d,%d,%d,%d)\n",yU11,xU11,mU11,nU11);
                       printf("U11(y,x,m,n)=(%d,%d,%d,%d)\n",yU11,xU11,mU11,nU11);
                       ImprimirMatriz(fp,U11,mU11,nU11);
                       fprintf(fp,"U21(y,x,m,n)=(%d,%d,%d,%d)\n",yU21,xU21,mU21,nU21);
                       printf("U21(y,x,m,n)=(%d,%d,%d,%d)\n",yU21,xU21,mU21,nU21);
                       ImprimirMatriz(fp,U21,mU21,nU21);
                       fprintf(fp,"Matriz U11U21\n");
                       printf("Matriz U11U21\n");
                       ImprimirMatriz(fp,U11U21,mA11A21,nA11A21);
                }
                TransponerMatriz(fp,U11,mU11,nU11,false); aux=mU11; mU11=nU11; nU11=aux;
                TransponerMatriz(fp,U21,mU21,nU21,false); aux=mU21; mU21=nU21; nU21=aux;
                InsertarSubmatriz(fp,UT11UT21, U11, mUT11UT21, nUT11UT21, 0,  0,    mU11, nU11,false);
                InsertarSubmatriz(fp,UT11UT21, U21, mUT11UT21, nUT11UT21, 0,  mU11, mU21, nU21,false);
                if (bDebug){
                       fprintf(fp,"Matriz UT11UT21\n");
                       printf("Matriz UT11UT21\n");
                       ImprimirMatriz(fp,UT11UT21,mUT11UT21,nUT11UT21);
                }
                free(U11);
		free(U21);
        }

        void ReflexionHouseholder(float * M, float * ref, unsigned long long  m,unsigned long long n){
                float * v = (float*)malloc(sizeof(float)*m);
                float * auxProd = (float*)malloc(sizeof(float)*m*n);
                memcpy(v,M,sizeof(float)*m);                                            //v=M(:,1);
                v[0]=v[0]+NormaVector(v,m);
		ProductoVectorEscalar(v, 1/NormaVector(v,m), m);                        //v=v*/norm(v)
                MatrizIdentidad(ref,m,m,1);                                             //ref=eye(size(m));
                cblas_sger(CblasColMajor, m, m, -2, v, 1, v, 1, ref, m);                //ref:= (-2)*v*vT + ref
                ProductoMatrices(CblasNoTrans, CblasNoTrans, ref, M, auxProd, m, n, m); //M=ref*m;
                memcpy(M,auxProd,sizeof(float)*m*n);
                free(v);
		free(auxProd);
        }

        void ActualizacionParcialHouseholder(float* origm, float * subm, unsigned long long  f,unsigned long long  m, unsigned long long n){
                //Copio la fila y la columna que no van a cambiar mas a la matriz origial
                memcpy(&origm[f+f*m],subm,sizeof(float)*(m-f));
                cblas_scopy(n-f,subm,m-f,&origm[f+f*m],m);
                if (m-f==2){//si es la ultima iteracion copio una fila mas
                      cblas_scopy(n-f,&subm[1],m-f,&origm[f+1+f*m],m);
                }
        }

        void HouseholderCPU(FILE * fp, float * M, unsigned long long m,unsigned long long  n, float* Q, bool Debug){
                float * subm = (float*)malloc(sizeof(float)*m*n);
                float * subq = (float*)malloc(sizeof(float)*m*m);
                float * qd = NULL;
                float * auxProd = NULL;
                if(Q){//El calculo de Q es opcional, si tiene memoria reservada se calcula
                         MatrizIdentidad(Q,m,m, 1); //q=q1*q2..*qn
                         qd = (float*)malloc(sizeof(float)*m*m);
                         auxProd = (float*)malloc(sizeof(float)*m*m);
                }
                ExtraerSubmatriz(fp,M,subm,m,n,0,0,m,n,Debug);
                for (unsigned long long d = 0; d < (m>n?n:m); d++){
                        if (m-d>1){
                                ReflexionHouseholder(subm,subq,m-d,n-d);
                                ActualizacionParcialHouseholder(M, subm, d,m,n);
                                if(Q){  
                                        MatrizIdentidad(qd,m,m,1);
                                        ActualizacionParcialHouseholder(qd, subq, d,m,m);
                                        ProductoMatrices(CblasNoTrans, CblasNoTrans, Q,qd, auxProd, m, m, m); //q=q*qd;
                                        memcpy(Q,auxProd,sizeof(float)*m*m);
                                }
                                if (Debug){
                                        fprintf(fp,"Reflexion en la columna [%d]:\n",d);
                                        printf("Reflexion en la columna [%d]:\n",d);
                                        ImprimirMatriz(fp,subm,m-d,n-d);
                                }
                                ExtraerSubmatriz(fp,subm, subm,m-d,n-d,1,1,m-d-1,n-d-1,Debug);
                        }
                }
                if (Debug){
                        fprintf(fp,"Matriz final de Householder\n");
                        printf("Matriz final de Householder\n");
                        ImprimirMatriz(fp,M,m,n);
                }
                if(Q){
                        free(qd);
			free(auxProd);
                }
                free(subm);
		free(subq);
       }

	void HouseholderUNB(FILE * fp, float * M, unsigned long long m, unsigned long long n, float * S, bool Debug){
		float * A22    = (float*)malloc(sizeof(float)*m*n);
		float * wt     = (float*)malloc(sizeof(float)*n);
		float * A21    = (float*)malloc(sizeof(float)*m);
		float * A11A21 = (float*)malloc(sizeof(float)*m);
		float * u      = (float*)malloc(sizeof(float)*m);
		float * At12   = (float*)malloc(sizeof(float)*n);
		float normaA11A21,normau,eta,sigma,v0,a11,aux;
		unsigned long long yA22,xA22,mA22,nA22,yA21,xA21,mA21,nA21,yA11A21,xA11A21,mA11A21,nA11A21,yAt12,xAt12,mAt12,nAt12;
		aux=1;
		for (unsigned long long i = 0; i < n; i++){
		        //    (a11 | At12)
		        //  M=(----+-----)
		        //    (A21 | A22 )

			//Extraigo submatrices
			a11=M[i+m*i];
                        yAt12=i;    xAt12=i+1;  mAt12=1;         nAt12=n-xAt12;
                        yA21=i+1;   xA21=i;     mA21=m-yA21;     nA21=1;
                        yA22=i+1;   xA22=i+1;   mA22=m-yA22;     nA22=n-xA22;
                        yA11A21=i;  xA11A21=i;  mA11A21=mA21+1;  nA11A21=1;
		
			ExtraerSubmatriz (fp,M,At12,   m,n, yAt12,   xAt12,   mAt12,    nAt12,   false);
			ExtraerSubmatriz (fp,M,A21,    m,n, yA21,    xA21,    mA21,     nA21,    false);
			ExtraerSubmatriz (fp,M,A22,    m,n, yA22,    xA22,    mA22,     nA22,    false);
			ExtraerSubmatriz (fp,M,A11A21, m,n, yA11A21, xA11A21, mA11A21,  nA11A21, false);
                        //Caculos
			normaA11A21 = NormaVector(A11A21, mA11A21);
			v0=a11+Signo(a11)*normaA11A21;
		        //u=vertcat(1,A21/v0);
			ProductoVectorEscalar(A21, 1/v0 , mA21); //A21=A21/v0
			u[0]=1;
			InsertarSubmatriz(fp,u,A21 ,mA11A21, nA11A21,1,0, mA21, nA21, Debug);

			normau=NormaVector(u,mA11A21);
    			sigma=2/(normau*normau);
    			eta=-Signo(a11)*normaA11A21;
        		a11=eta;
        		//atualizo A21 a partir de u
			ExtraerSubmatriz (fp,u,A21, mA21+1, 1, 1, 0,  mA21, nA21,Debug);
			if (Debug){
		        	printf("At12(y,x,m,n)=(%d,%d,%d,%d)\n",yAt12,xAt12,mAt12,nAt12);
		        	fprintf(fp,"At12(y,x,m,n)=(%d,%d,%d,%d)\n",yAt12,xAt12,mAt12,nAt12);
		        	printf("A21 (y,x,m,n)=(%d,%d,%d,%d)\n",yA21, xA21, mA21, nA21);
		        	fprintf(fp,"A21 (y,x,m,n)=(%d,%d,%d,%d)\n",yA21, xA21, mA21, nA21);
		        	printf("A22 (y,x,m,n)=(%d,%d,%d,%d)\n",yA22, xA22, mA22, nA22);
		        	fprintf(fp,"A22 (y,x,m,n)=(%d,%d,%d,%d)\n",yA22, xA22, mA22, nA22);
		        	printf("A11A21(y,x,m,n)=(%d,%d,%d,%d)\n",yA11A21, xA11A21, mA11A21, nA11A21);
		        	fprintf(fp,"A11A21(y,x,m,n)=(%d,%d,%d,%d)\n",yA11A21, xA11A21, mA11A21, nA11A21);
		        	printf("a11[%f], normaA11A21[%f], v0[%f], sigma[%f], eta[%f]\n",a11, normaA11A21, v0, sigma, eta);
		        	fprintf(fp,"a11[%f], normaA11A21[%f], v0[%f], sigma[%f], eta[%f]\n",a11, normaA11A21, v0, sigma, eta);
			}
        		if (mA22>0 && nA22>0){
            			//wt=at12+(A21'*A22);
				InsertarSubmatriz(fp,wt,At12 ,mAt12,nAt12, 0, 0, mAt12, nAt12, false);//wt=at12
				ProductoMatrices(CblasNoTrans, CblasNoTrans, A21, A22, wt, nA21, nA22, mA21,1,1);//wt=wt+A21'*A22
	        	    	//at12=-sigma*wt+at12
				cblas_sgemv(CblasColMajor,CblasNoTrans, nAt12, 1, -sigma, wt, nAt12, &aux,1, 1, At12, 1);
        	 	    	//A22=A22-sigma*A21*wt;
				ProductoMatrices(CblasNoTrans, CblasNoTrans, A21, wt, A22, mA21, nAt12, nA21,-sigma,1);
				if (Debug){
					printf("At12 actualizada:\n");
					fprintf(fp,"At12 actualizada:\n");
	        			ImprimirMatriz(fp,At12,mAt12,nAt12);
					printf("A22 actualizada:\n");
					fprintf(fp,"A22 actualizada:\n");
	        			ImprimirMatriz(fp,A22,mA22,nA22);
	        		}
        		} 
			//Aplico los cambios
			M[i+i*m]=a11;                      
			InsertarSubmatriz(fp,M,At12 ,m,n, yAt12, xAt12, mAt12, nAt12, Debug);
			InsertarSubmatriz(fp,M,A21  ,m,n, yA21,  xA21,  mA21,  nA21,  Debug);
			InsertarSubmatriz(fp,M,A22  ,m,n, yA22,  xA22,  mA22,  nA22,  Debug);
			S[i]=sigma;
		}		
		if (Debug){
			fprintf(fp,"Resultado de HouseholderUNB\n");
		        printf("Resultado de HouseholderUNB\n");
	        	ImprimirMatriz(fp,M,m,n);
		}
                free(A22);
		free(wt);
		free(A21);
		free(A11A21);
		free(u);
		free(At12);
	}

