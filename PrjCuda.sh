export PATH=/usr/lib64/qt-3.3/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/ens/home01/a/agustin.young/bin:/usr/local/cuda/bin:/opt/cuda-6.0/bin/
export LD_LIBRARY_PATH=/usr/local/cuda/lib64:/opt/cuda-6.0/lib64
#export PATH=$(PATH):/usr/local/cuda/bin
#export LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):/usr/local/cuda/lib64
make
clear
case $# in
0) ./PrjCuda;;
1) ./PrjCuda $1;;
2) ./PrjCuda $1 $2;;
3) ./PrjCuda $1 $2 $3;;
4) ./PrjCuda $1 $2 $3 $4;;
5) ./PrjCuda $1 $2 $3 $4 $5;;
*) echo 'Cantidad incorrecta de parametros'; ;;
esac;

