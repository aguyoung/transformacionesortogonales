
#include <stdio.h>
#include "lapacke.h"
#include "Auxiliares.h"
#include <string.h>
#include "math.h"

        void DefinirRotacion(unsigned long long i,unsigned long long j,float angulo,float * rotacion,unsigned long long m){
                rotacion[i + i * m]=cos(angulo);
                rotacion[j + j * m]=cos(angulo);
                rotacion[i + j * m]=sin(angulo);
                rotacion[j + i * m]=-sin(angulo);
        }

        void RotacionConVectores(unsigned long long f1, unsigned long long f2, float * M,float angulo, unsigned long long m, unsigned long long n, unsigned long long l){
                float * r = (float*)malloc(sizeof(float)*n*2);
                //calculo los vectores rotados
                for (int x = 0; x<n; x++){
                r[x]=   M[f1+x*l]*cos(angulo)+M[f2+x*l]*(-sin(angulo));
                r[x+n]= M[f2+x*l]*cos(angulo)+M[f1+x*l]*sin(angulo);
                }
                //copio los vectores en la matriz original
                for (int x = 0; x<n; x++){
                    M[f1+x*l]=r[x];
                    M[f2+x*l]=r[x+n];
                }
		free(r);
        }

        void GivensCPU(FILE * fp, float * M, float * q, unsigned long long m,unsigned long long  n, unsigned long long l, bool Debug){
                float * rotacion = (float*)malloc(sizeof(float)*m*m);
                float * auxProd = (float*)malloc(sizeof(float)*m*m);
                float angulo;
		if(q){
			MatrizIdentidad(q,m,m, 1); //q=I*q1*q2..*qn
		}
                for (unsigned long long x = 0; x < n ; x++){
                        for (unsigned long long y = x+1; y < m ; y++){
                                if (M[y+x*l]!=0){
                                        unsigned long long auxRot = x;
                                        bool encontro =false;
                                        while ( auxRot < m && !encontro){
                                                if (M[auxRot+x*l]!=0 && auxRot!=y){
                                                        angulo=atan(M[y+x*l]/M[auxRot+x*l]);
                                                        RotacionConVectores(y,auxRot,M,angulo,m,n,l);
                                                        if (Debug){
                                                                printf("Pondra un cero en [%f] con angulo calculado atan( %f div %f )= %f \n",M[y+x*l],M[y+x*l],M[auxRot+x*l],angulo);
                                                                fprintf(fp,"Pondra un cero en [%f] con angulo calculado atan( %f div %f )= %f \n",M[y+x*l],M[y+x*l],M[auxRot+x*l],angulo);
                                                                ImprimirMatriz(fp,M,m,n,l);
                                                        }
							//Si hay que calcular la matriz q
							if (q){
								//q=rot*q;
								MatrizIdentidad(rotacion,m,m, 1);
                                                        	DefinirRotacion(auxRot,y,angulo,rotacion,m);
                                                        	ProductoMatrices(CblasNoTrans, CblasNoTrans, rotacion, q,auxProd, m, m, m);
                                                        	memcpy(q,auxProd,sizeof(float)*m*m);
								if (Debug){
									printf("matriz rot:\n");
									fprintf(fp,"matriz rot:\n");
                                                                	ImprimirMatriz(fp,rotacion,m,m);
									printf("matriz q:\n");
									fprintf(fp,"matriz q:\n");
                                                                	ImprimirMatriz(fp,q,m,m);
                                                                }
							}
                                                        encontro=true;
                                                }
                                                auxRot++;
                                        }
                                }
                        }
                }
		free(rotacion);
		free(auxProd);
        }


