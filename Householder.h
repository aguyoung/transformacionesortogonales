#ifndef __HOUSEHOLDER_H
#define __HOUSEHOLDER_H

void HouseholderCPU(FILE * fp, float * M, unsigned long long m,unsigned long long  n, float * Q, bool Debug);
void HouseholderUNB(FILE * fp, float * M, unsigned long long m, unsigned long long n, float * S, bool Debug);
void ExtraerSubmatricesEnterasU (FILE * fp, float * A11A21, float * U11U21, float * UT11UT21, unsigned long long mA11A21, unsigned long long nA11A21,  unsigned long long mA11, bool bDebug);

#endif
