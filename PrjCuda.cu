	#define _USE_MATH_DEFINES //para poder usar las constantes matematicas
	#include "cuda.h"
	#include "cuda_runtime.h"
	#include "device_launch_parameters.h"
	#include "math.h"
	#include "curand.h"
	#include "curand_kernel.h"
	#include "cblas.h"
	#include "cublas.h"
	#include <stdio.h>
	#include <stdlib.h>
	//#include <Windows.h>
	#include "Givens.h"
	#include "Householder.h"
	#include "Hessenberg.h"
	#include "Auxiliares.h"
	#include <time.h>
	using namespace std;
	#define cSemilla 1
	#define cGuardarEnParametro1 1
	#define cGuardarEnParametro2 2
	#define cDefaultIndiceCantidadHilosGivens 3
	#define cDefaultIndiceCantidadHilosHouseholder 5
	#define cDimBloqueGivens 8
	#define cDimBloqueHouseholder 4
	#define cTolerancia 0.0001f
	#define cFun_GivensCPU          "GC"
	#define cFun_GivensGPU_1        "GG1"
	#define cFun_GivensGPU_2        "GG2"
	#define cFun_GivensHibrido      "GH"
	#define cFun_HouseholderCPU     "HC"
	#define cFun_HouseholderGPU     "HG"
	#define cFun_HouseholderHibrido "HH"
	#define cFun_HessenbergCPU      "SC"
	#define cFun_TestHilos          "TH"
	#define cFun_TestRendimiento    "TR"
	#define cFun_TestMatrices       "TM"
	#define cFun_TestBloques        "TB"
	#define CUDA_CALL(x) \
		{ \
		cudaError_t err = x; \
		if(cudaSuccess != err) \
			printf("\n\n\nERROR CUDA Error on call \"%s\": %s\n\tLine: %d, File: %s\n\n\n", \
					#x, cudaGetErrorString(err), __LINE__, __FILE__); \
		}

	//variables globales	
	struct timespec ctr1, ctr2;
	//int64_t ctr1 = 0;int64_t ctr2 = 0;int64_t freq = 0; //para windows
	//__int64 ctr1 = 0;__int64 ctr2 = 0;__int64 freq = 0; //para windows
	int CantidadHilos[]={4, 8, 16, 32, 64, 128, 256, 512, 1024};
	float duracion =0;
	FILE * fp;
	bool bPrimerLog = true;
	float ProbabilidadCero=0.0001;

	void clockStart(){
		//en linux
		clock_gettime(2,&ctr1);	

		//en windows
		//QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
	}

	float clockStop(){
		//en linux
		clock_gettime(2,&ctr2);
		float aux = (ctr2.tv_sec-ctr1.tv_sec)*1000000000 +ctr2.tv_nsec-ctr1.tv_nsec;
		return aux/1000000000.0f;
		
		//en windows
		//QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		//QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		//return (ctr2 - ctr1) * 1.0 / freq;
		
	}


        void DescargarSubmatrizGPU(FILE * fp, float* D, float * H, unsigned long long Dm, unsigned long long  Dn,unsigned long long  Hy,unsigned long long  Hx, unsigned long long Hm, unsigned long long Hn, unsigned long long Hl,bool bDebug){
                for (unsigned long long c = 0; c < Hn; c++){
			CUDA_CALL(cudaMemcpy(&H[c*Hl], &D [(c+Hx)*Dm + Hy], Hm*sizeof(float), cudaMemcpyDeviceToHost));
                }
                if (bDebug){
                        printf("Submatiz descargada con coordenadas de punto inicial (%d,%d), dim(%d,%d)\n",Hy,Hx,Hm,Hn);
                        fprintf(fp,"Submatiz descargada con coordenadas de punto inicial (%d,%d), dim(%d,%d)\n",Hy,Hx,Hm,Hn);
                        ImprimirMatriz(fp,H,Hm,Hn);
                }
        }
        
	void CopiarSubmatrizGPU(FILE * fp, float* O, float * D, unsigned long long Om, unsigned long long  On,unsigned long long  Dy,unsigned long long  Dx, unsigned long long Dm, unsigned long long Dn, unsigned long long Dl){
                for (unsigned long long c = 0; c < Dn; c++){
			CUDA_CALL(cudaMemcpy(&D [c*Dl], &O [(c+Dx)*Om + Dy], Dm*sizeof(float), cudaMemcpyDeviceToDevice));
                }
        }
        
	void IprimirSubmatrizGPU(FILE * fp, float* D, unsigned long long Dm, unsigned long long  Dn,unsigned long long  Sy,unsigned long long  Sx, unsigned long long Sm, unsigned long long Sn){
		float * S = (float*)malloc(sizeof(float)*Sm*Sn);
        	DescargarSubmatrizGPU(fp, D, S, Dm, Dn, Sy, Sx, Sm, Sn, Sm, false);
                printf("Submatiz en GPU con coordenadas de punto inicial (%d,%d), dim(%d,%d)\n",Sy,Sx,Sm,Sn);
                fprintf(fp,"Submatiz en GPU con coordenadas de punto inicial (%d,%d), dim(%d,%d)\n",Sy,Sx,Sm,Sn);
                ImprimirMatriz(fp,S,Sm,Sn);
		free(S);
	}


	void HouseholderHibrido(float * M, unsigned long long Mm, unsigned long long Mn, unsigned long long maxDimBloque, unsigned long long  indiceCantidadHilos, bool Debug){
		size_t TamanoDeMatriz = sizeof(float)*Mm*Mn;
		float * Md;
		float * UT11UT21d;
		float * A1d;
		float * A2d;
		float * A12A22d;
		float * A11A21 =   (float*)malloc(sizeof(float)*Mm*maxDimBloque);
		float * U11U21 =   (float*)malloc(sizeof(float)*Mm*maxDimBloque);
		float * UT11UT21 = (float*)malloc(sizeof(float)*Mm*maxDimBloque);
		float * Maux =     (float*)malloc(sizeof(float)*Mm*maxDimBloque);
		float * s1 = (float*)malloc(sizeof(float)*maxDimBloque);
		float * Si = (float*)malloc(sizeof(float)*maxDimBloque);
		float * Sb = (float*)malloc(sizeof(float)*maxDimBloque*maxDimBloque);
                unsigned long long minmn = Mm>Mn?Mn:Mm;
		unsigned long long cantBloques = minmn/maxDimBloque;
		unsigned long long yA11,xA11,mA11,nA11,yA11A21,xA11A21,mA11A21,nA11A21,yA12A22,xA12A22,mA12A22,nA12A22;
		
		//Copio la matriz original al device
		CUDA_CALL(cudaMalloc(&Md, TamanoDeMatriz));
		CUDA_CALL(cudaMalloc(&A1d, TamanoDeMatriz));
		CUDA_CALL(cudaMalloc(&A2d, TamanoDeMatriz));
		CUDA_CALL(cudaMalloc(&UT11UT21d, TamanoDeMatriz));
		CUDA_CALL(cudaMemcpy(Md, M, TamanoDeMatriz, cudaMemcpyHostToDevice));
		
                if (minmn%maxDimBloque> 0){
                    cantBloques++;
		}
		for (unsigned long long bloque = 0; bloque < cantBloques; bloque++){
		        //    (A11|A12)
		        //  M=(---+---)
		        //    (A21|A22)
			//determino las coordanadas y demensiones de A11
			if (cantBloques==1){
				mA11=minmn;
			}else{
				if(bloque+1==cantBloques){
					mA11=minmn%maxDimBloque==0?maxDimBloque:minmn%maxDimBloque;
				}else{
					mA11=maxDimBloque;
				}
			}
                        yA11=bloque*maxDimBloque;    xA11=yA11;                          nA11=mA11;
                        yA11A21=yA11;                 xA11A21=xA11;       mA11A21=Mm-yA11A21; nA11A21=nA11;
                        yA12A22=yA11;                 xA12A22=xA11+nA11;  mA12A22=mA11A21;   nA12A22=Mn-xA12A22; A12A22d=Md + xA12A22*Mm+yA12A22;
			if (yA11A21>1){
				DescargarSubmatrizGPU (fp,Md+Mm*xA11A21,&M[Mm*xA11A21],Mm,Mn, 0,  0, yA11A21, nA11A21,Mm,Debug);//Descargo la parte que ya no cambia
			}
			DescargarSubmatrizGPU (fp,Md,A11A21,Mm,Mn, yA11A21,  xA11A21, mA11A21, nA11A21,mA11A21,Debug);
			if (Debug){
		        	printf("A11A21(y,x,m,n)=(%d,%d,%d,%d)\n",yA11A21,xA11A21,mA11A21,nA11A21);
		        	fprintf(fp,"A11A21(y,x,m,n)=(%d,%d,%d,%d)\n",yA11A21,xA11A21,mA11A21,nA11A21);
	        		ImprimirMatriz(fp,A11A21,mA11A21,nA11A21);
			}
			HouseholderUNB(fp,A11A21, mA11A21,nA11A21,s1,Debug);
			if (Debug){
				printf("Resultado UNB:");
				fprintf(fp,"Resultado UNB:");
	        		ImprimirMatriz(fp,A11A21,mA11A21,nA11A21);
			}
			InsertarSubmatriz(fp,M,A11A21,Mm,Mn,yA11A21,  xA11A21, mA11A21, nA11A21,false);

			if (xA12A22<Mn){//si hay que actualizar a la derecha
				ExtraerSubmatricesEnterasU(fp, A11A21, U11U21, UT11UT21, mA11A21, nA11A21, mA11, Debug);
				Sb[0]=-s1[0];
				for (int i = 1; i<nA11;i++){
					//Si=(-s(i)*Si'*A11A21(:,1:i-1)'*U11U21(:,i:i))';
					cblas_sgemm(CblasColMajor, CblasTrans, CblasTrans, i, mA11A21, i, -s1[i], Sb ,nA11, A11A21, mA11A21, 0, Maux, i);//Maux=-s[i]*Si'A11A21(:,1:i-1)'
					cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, i, 1, mA11A21, 1, Maux ,i, &U11U21[i*mA11A21], mA11A21, 0, Si, nA11);//Si'=Mxux*U11U21(:,i:i)
					InsertarSubmatriz(fp,Sb,Si,nA11,nA11,i,0,1,i,false); //Sb(i:i,1:i-1)=Si';
					Sb[i+i*nA11]=-s1[i];//Sb(i,i)=-s(i);
				}
				if (Debug){
		        		printf("Sb:\n");
		        		fprintf(fp,"Sb:\n");
	        			ImprimirMatriz(fp,Sb,nA11,nA11);
				}
				ProductoMatrices(CblasNoTrans, CblasNoTrans, U11U21, Sb,Maux, mA11A21, nA11A21 , nA11A21);//A1=U11U21*Sb
				CUDA_CALL(cudaMemcpy(A1d       , Maux    , mA11A21 * nA11A21 * sizeof(float) , cudaMemcpyHostToDevice));
				CUDA_CALL(cudaMemcpy(UT11UT21d, UT11UT21, nA11A21 * mA11A21 * sizeof(float) , cudaMemcpyHostToDevice));
					
				cublasSgemm('N','N',nA11A21,nA12A22,mA12A22,1,UT11UT21d,nA11A21,A12A22d,Mm,0,A2d,nA11A21);//A2d=UT11UT21*A12A22
				cublasSgemm('N','N',mA12A22,nA12A22,nA11A21,1,A1d,mA12A22,A2d,nA11A21,1,A12A22d,Mm);//A12A22 = A1d*A2d + A12A22
				if (bloque+1==cantBloques){
					if (Debug){
						printf("Descargo el la submatriz actualizada \n");
						fprintf(fp,"Descargo el la submatriz actualizada \n");
					}
					DescargarSubmatrizGPU(fp,Md,&M[Mm*xA12A22],Mm,Mn, 0, xA12A22, Mm, nA12A22,Mm,Debug);//Tengo que descargar el ultimo bloque A12A22
				}
                        }
		}
		if (Debug){
			fprintf(fp,"Matriz final de Householder\n");
		        printf("Matriz final de Householder\n");
	        	ImprimirMatriz(fp,M,Mm,Mn);
		}
                free(A11A21);
		free(U11U21);
		free(UT11UT21);
		free(Maux);
		free(s1);
		free(Si);
		free(Sb);
		cudaFree(Md);
		cudaFree(A1d);
		cudaFree(A2d);
		cudaFree(UT11UT21d);
	}

	void GivensHibrido(float * M, unsigned long long Mm, unsigned long long Mn, unsigned long long maxDimBloque,unsigned long long  indiceCantidadHilos, bool Debug){
		size_t TamanoDeM = sizeof(float)*Mm*Mn;
		size_t TamanoDeC = sizeof(float)*maxDimBloque*Mn*2;
		size_t TamanoDeB = sizeof(float)*maxDimBloque*maxDimBloque*2;
		size_t TamanoDeQ = sizeof(float)*maxDimBloque*maxDimBloque*4;
		float * Md;
		float * Cd;
		float * Ad;
		float * Qd;
		float * Aux;
		float * B   = (float*)malloc(TamanoDeB);
		float * Q   = (float*)malloc(TamanoDeQ);
		unsigned long long Bm,Bn,Bl,B1y,B1x,B1m,B1n,B2y,B2x,B2m,B2n,Cm,Cn,Cl,C1y,C1x,C1m,C1n,C2y,C2x,C2m,C2n;
		B1x=0;
		B1y=0;
		Bl=maxDimBloque*2;
		Cl=Bl;
		CUDA_CALL(cudaMalloc(&Md, TamanoDeM));
		CUDA_CALL(cudaMalloc(&Cd, TamanoDeC));
		CUDA_CALL(cudaMalloc(&Ad, TamanoDeC));
		CUDA_CALL(cudaMalloc(&Qd, TamanoDeQ));
		CUDA_CALL(cudaMemcpy(Md, M, sizeof(float)*Mm*Mn, cudaMemcpyHostToDevice));
		while (B1x<Mn && B1y<Mm){
        		if (B1x+maxDimBloque <= Mn)
            			B1n=maxDimBloque;
        		else
            			B1n=Mn-B1x;
        		if (B1y+maxDimBloque <= Mm)
            			B1m=maxDimBloque;
        		else
            			B1m=Mm-B1y;
			if (B1x>0){
				DescargarSubmatrizGPU (fp,Md,&M[Mm*B1x],Mm,Mn, 0,  B1x, Mm, B1n,Mm,Debug);//Descargo la zona de columnas B actualizada
			}
			ExtraerSubmatriz (fp,M,B,Mm,Mn, B1y,B1x, B1m, B1n,Bl,false);
			if (Debug){
		        	printf("B1(y,x,m,n)=(%d,%d,%d,%d)\n",B1y,B1x,B1m,B1n);
		        	fprintf(fp,"B1(y,x,m,n)=(%d,%d,%d,%d)\n",B1y,B1x,B1m,B1n);
	        		ImprimirMatriz(fp,B,B1m,B1n,Bl);
			}
			C1x=B1x+B1n;
			//Si hay que actualizar a la derecha de B1
			if(C1x<=Mn){
				C1y=B1y;
                		C1m=B1m;
             			C1n=Mn-B1x-B1n;
				CopiarSubmatrizGPU(fp,Md,Cd    ,Mm,Mn, C1y,  C1x, C1m, C1n,Cl);
				if (Debug){
		        		printf("C1(y,x,m,n)=(%d,%d,%d,%d)\n",C1y,C1x,C1m,C1n);
		        		fprintf(fp,"C1(y,x,m,n)=(%d,%d,%d,%d)\n",C1y,C1x,C1m,C1n);
	        			IprimirSubmatrizGPU(fp,Cd,Cl,C1n,0,0,C1m,C1n);
				}
			}
        		B2x=B1x;
		     	B2y=B1x+B1m;
	               	B2n=B1n;
	       		do{
	           		if (B2y+maxDimBloque <= Mm)
	               			B2m=maxDimBloque;
	        		else
	               			B2m=Mm-B2y;
				Bm=B1m+B2m;
				Bn=B1n;
			   	ExtraerSubmatriz (fp,M,&B[B1m],Mm,Mn, B2y,  B2x, B2m, B2n,Bl,false);
				if (Debug){
		        		printf("B2(y,x,m,n)=(%d,%d,%d,%d)\n",B2y,B2x,B2m,B2n);
		        		fprintf(fp,"B2(y,x,m,n)=(%d,%d,%d,%d)\n",B2y,B2x,B2m,B2n);
	        			ImprimirMatriz(fp,&B[B1m],B2m,B2n,Bl);
				}
				GivensCPU(fp,B,Q,Bm,Bn,Bl,false);
				if (Debug){
					printf("Matriz B actualizada:\n");
					fprintf(fp,"Matriz B actualizada:\n");
	        			ImprimirMatriz(fp,B,Bm,Bn);
					printf("Matriz Q:\n");
					fprintf(fp,"Matriz Q:\n");
	        			ImprimirMatriz(fp,Q,Bm,Bm);
				}
			   	ExtraerSubmatriz (fp,B,&M[B2y+B2x*Mm],Bl,B2n,B1m,0,B2m,B2n,Mm,false);
				if(C1x<=Mn){
					CUDA_CALL(cudaMemcpy(Qd, Q, Bm*Bm*sizeof(float), cudaMemcpyHostToDevice));
                			C2x=C1x;
                			C2y=B2y;
                			C2m=B2m;
                			C2n=C1n;
					if (Debug){
		        			printf("C2(y,x,m,n)=(%d,%d,%d,%d)\n",C2y,C2x,C2m,C2n);
		        			fprintf(fp,"C2(y,x,m,n)=(%d,%d,%d,%d)\n",C2y,C2x,C2m,C2n);
	        				IprimirSubmatrizGPU(fp,Md,Mm,Mn,C2y,C2x,C2m,C2n);
					}
					Cm=Bm;
					Cn=C1n;
					CopiarSubmatrizGPU(fp,Md,&Cd[C1m],Mm,Mn, C2y,  C2x, C2m, C2n,Cl);
					if (Debug){
		        			printf("C(y,x,m,n)=(%d,%d,%d,%d)\n",0,0,Cm,Cn);
		        			fprintf(fp,"C(y,x,m,n)=(%d,%d,%d,%d)\n",0,0,Cm,Cn);
	        				IprimirSubmatrizGPU(fp,Cd,Cl,Cn,0,0,Cm,Cn);
					}
					cublasSgemm('N','N',Cm,Cn,Cm,1,Qd,Cm,Cd,Cl,0,Ad,Cl);//A=Q*C
					//Hago un intecambio de punteros Entre Ad y Cd para evitarme copiar C1
					Aux=Ad;
					Ad=Cd;
					Cd=Aux;
					if (Debug){
						printf("Matriz C actualizada:\n");
						fprintf(fp,"Matriz C actualizada:\n");
	        				IprimirSubmatrizGPU(fp,Cd,Cl,Cn,0,0,Cm,Cn);
					}
					CopiarSubmatrizGPU(fp,Cd,&Md[C2y+C2x*Mm],Cl,Cn, C1m, 0, C2m, C2n,Mm);//Copio C2 actualizada en M 
					if (Debug){
						printf("Matriz M actualizada en GPU:\n");
						fprintf(fp,"Matriz M actualizada en GPU:\n");
	        				IprimirSubmatrizGPU(fp,Md,Mm,Mn,0,0,Mm,Mn);
					}
				}
				//Muevo B2
				if (B2m==0){
					B2m=1;//Fuezo a salir del bucle
				}
				B2y=B2y+B2m;
			}while (B2y<Mm);
			if(C1x<Mn){
				CopiarSubmatrizGPU(fp,Cd,&Md[C1y+C1x*Mm],Cl,Cn, 0,  0, C1m, C1n,Mm);//Copio C1 actualizada en M
			}
			ExtraerSubmatriz (fp,B,&M[B1y+B1x*Mm],Bl,B1n,0, 0, B1m,B1n,Mm,false);//Actualizo B1
			B1y=B1y+B1m;
			B1x=B1x+B1n;
		}		
		if (B1x<Mn){	
			DescargarSubmatrizGPU(fp,Md,&M[Mm*B1x],Mm,Mn, 0, B1x, Mm, Mn-B1x,Mm,Debug);//Tengo que descargar la parte de la derecha que solo se actualizo
		}
		if (Debug){
			fprintf(fp,"Matriz final de Givens a bloques\n");
		        printf("Matriz final de Given a bloques\n");
	        	ImprimirMatriz(fp,M,Mm,Mn);
		}
                free(B); 
		free(Q);
		cudaFree(Md);
		cudaFree(Cd);
		cudaFree(Ad);
		cudaFree(Qd);
	}

	__global__ void KernelGivens1(unsigned long long m, unsigned long long n, float * M, float * t, unsigned long long * plan){

		float angulo;
		unsigned long long  f1;
		unsigned long long  f2;
		unsigned long long  aux;
		unsigned long long  ini;
		unsigned long long  fin;
		unsigned long long  cont;
		unsigned long long  c1;
		unsigned long long  c2;
		bool bEjecutar=false;

		if (blockIdx.x < plan[0]/2){
			//identifico filas y rangos
			c1 = plan[blockIdx.x*2+1+(plan[0]/2)*2+(plan[0]%2)*2];
			f1 = plan[blockIdx.x*2+2+(plan[0]/2)*2+(plan[0]%2)*2];
			c2 = plan[blockIdx.x*2+1];
			f2 = plan[blockIdx.x*2+2];
			if (c1==f1){//si la cela que le va a poner el cero es una de la diagonal la intercambio con la otra
				aux=c1;
				c1=c2;
				c2=aux;
				aux=f1;
				f1=f2;
				f2=aux;
			}
			angulo = atan(M[f1+c1*m]/M[f2+c2*m]);
			
			cont = n/blockDim.x;
			if (n%blockDim.x>0) cont ++;
			ini=threadIdx.x*cont;
			fin=ini+cont;
			if (c1 > ini) ini =c1;
			if (fin > n) fin =n;
			if (c1<f1) bEjecutar=true;

		}
		__syncthreads();
				
		if (bEjecutar){
			//calculo la rotacion
			for (int i = ini; i<fin; i++){
				t[f1+i*m]= M[f1+i*m]*cos(angulo)+M[f2+i*m]*(-sin(angulo));
				t[f2+i*m]= M[f2+i*m]*cos(angulo)+M[f1+i*m]*sin(angulo);
			}
		}
			
		__syncthreads();
				
		if (bEjecutar){
			//copio los vectores en la matriz original
			for (int i = ini; i<fin; i++){
				M[f1+i*m]=t[f1+i*m];
				M[f2+i*m]=t[f2+i*m];
			}
		}

		__syncthreads();
				
	}

	__global__ void KernelPlan1(unsigned long long n, unsigned long long x, float * m, unsigned long long * plan){

		unsigned long long thid = threadIdx.x + blockDim.x * blockIdx.x;
		unsigned long long tmp;
		//Si la poscion es mayor a la tolerancia se agrega al plan. El elemento de la diagonal siempre va
		if ((thid<n && thid>=x && fabs(m[thid+x*n])>=cTolerancia)||(x==thid)){
			tmp=atomicAdd(&plan[0], 1);
			plan[tmp*2+1]=x;
			plan[tmp*2+2]=thid; 
		}

	}


	void GivensGPU1(float * resH, unsigned long long m, unsigned long long n,int indiceCantidadHilos,bool Debug){
		size_t TamanoDeMatriz = sizeof(float)*m*n;
		size_t TamanoDePlanficacion = sizeof(unsigned long long)*(m*2+1);

		float * resD;//Matriz resultado
		float * tempD;//Matriz auxiliar para las rotaciones
		unsigned long long  * planD;//planificacion: lista con posiciones en la columna con valor distinto de cero. Priemer pos es el largo.  
		unsigned long long  * planH = (unsigned long long*)malloc(TamanoDePlanficacion);

		// Allocate en device 
		CUDA_CALL(cudaMalloc(&resD, TamanoDeMatriz));
		CUDA_CALL(cudaMalloc(&tempD, TamanoDeMatriz));
		CUDA_CALL(cudaMalloc(&planD, TamanoDePlanficacion));
		
		// Inicializo matrices en el device
		CUDA_CALL(cudaMemcpy(resD, resH, TamanoDeMatriz, cudaMemcpyHostToDevice));
		CUDA_CALL(cudaMemset(planD,0,TamanoDePlanficacion));

		// Lanzo el kernel principal
		dim3 tamGrid(m/2);
		dim3 tamBlock(CantidadHilos[indiceCantidadHilos]);

		for (unsigned long long x=0; x<n; x++){
			planH[0]=2; //para que entre una vez al while
			while (planH[0] > 1){
				CUDA_CALL(cudaMemset(planD,0,sizeof(unsigned long long)));//planH[0]=0;
				CUDA_CALL(cudaThreadSynchronize());
				KernelPlan1<<<tamGrid, tamBlock>>>(m,x,resD,planD);
				CUDA_CALL(cudaThreadSynchronize());
				CUDA_CALL(cudaMemcpy(planH, planD, sizeof(unsigned long long), cudaMemcpyDeviceToHost));
				CUDA_CALL(cudaThreadSynchronize());
				if (planH[0] > 1){
					KernelGivens1<<<tamGrid, tamBlock>>>(m,n,resD,tempD,planD);
					CUDA_CALL(cudaThreadSynchronize());
					if(Debug){ 
						CUDA_CALL(cudaMemcpy(planH, planD, TamanoDePlanficacion, cudaMemcpyDeviceToHost));
						CUDA_CALL(cudaThreadSynchronize());
						ImprimirPlan1(fp,planH);
						CUDA_CALL(cudaMemcpy(resH, resD, TamanoDeMatriz, cudaMemcpyDeviceToHost));
						CUDA_CALL(cudaThreadSynchronize());
						ImprimirMatriz(fp,resH, m,n);
					}
				}
			}
		}
		// Traer resultado;
		CUDA_CALL(cudaMemcpy(resH, resD, TamanoDeMatriz, cudaMemcpyDeviceToHost));
		CUDA_CALL(cudaThreadSynchronize());

		free(planH);
		// Free matrices en device
		cudaFree(resD); 
		cudaFree(tempD); 
		cudaFree(planD); 
	}

	__global__ void KernelGivens2(unsigned long long m,unsigned long long n, float * M, float * t, unsigned long long * plan){

		float angulo;
		unsigned long long  f1;
		unsigned long long  f2;
		unsigned long long  aux;
		unsigned long long  ini;
		unsigned long long  fin;
		unsigned long long  cont;
		unsigned long long  c1;
		bool bEjecutar=false;

		if (blockIdx.x < plan[0]){
			//identifico filas y rangos
			c1 = plan[blockIdx.x*3+1];
			f1 = plan[blockIdx.x*3+2];
			f2 = plan[blockIdx.x*3+3];
			if (f2>f1){//los intercambio para que pongo el cero en las fila mas altas
				aux=f1;
				f1=f2;
				f2=aux;
			}
			angulo = atan(M[f1+c1*m]/M[f2+c1*m]);
			
			cont = n/blockDim.x;
			if (n%blockDim.x>0) cont ++;
			ini=threadIdx.x*cont;
			fin=ini+cont;
			if (c1 > ini) ini =c1;
			if (fin > n) fin =n;
			bEjecutar=true;

		}
		__syncthreads();
				
		if (bEjecutar){
			//calculo la rotacion
			for (int i = ini; i<fin; i++){
				t[f1+i*m]= M[f1+i*m]*cos(angulo)+M[f2+i*m]*(-sin(angulo));
				t[f2+i*m]= M[f2+i*m]*cos(angulo)+M[f1+i*m]*sin(angulo);
			}
		}
			
		__syncthreads();
				
		if (bEjecutar){
			//copio los vectores en la matriz original
			for (int i = ini; i<fin; i++){
				M[f1+i*m]=t[f1+i*m];
				M[f2+i*m]=t[f2+i*m];
			}
		}

		__syncthreads();
				
	}


	__global__ void KernelPlan2_M(unsigned long long m,unsigned long long n, float * M, unsigned long long * Mplan,unsigned long long * pivotes){
		//Este kernel caga la matriz Mplan
		unsigned long long thid = threadIdx.x + blockDim.x * blockIdx.x;
		unsigned long long iMplan;
		unsigned long long x=0;

		if(thid<m){
			
			x=pivotes[thid];

			if(x>0 && fabs(M[thid+(x-1)*m])>=cTolerancia){//Verifico que el anterior efectivamente no supere la toleracia, sino reseteo el pivote
				x=0;
			}

                	//me paro en la primer columna distinto de cero
                	while (x<thid && x<n && fabs(M[thid+x*m])<cTolerancia){
                	        x++;
                	}
			
			pivotes[thid]=x;

			//Si hay que rotar la agrego al plan 
			if ((x<=thid && x<n && fabs(M[thid+x*m])>=cTolerancia)||(x==thid)){
				iMplan=atomicAdd(&Mplan[x], 1);//pido indice
				iMplan++;//me paro en la posicion
				Mplan[x+iMplan*n]=thid;//guardo el numero de fila en el Mplan
				pivotes[m+thid]=iMplan;//guardo el indice en el Mplan
			}
		}
	}
		
	__global__ void KernelPlan2_V(unsigned long long m,unsigned long long n, float * M, unsigned long long * Vplan,unsigned long long * Mplan,unsigned long long * pivotes){
		//Este kernel arma el el vector VPlan
		unsigned long long thid = threadIdx.x + blockDim.x * blockIdx.x;
		unsigned long long iMplan;
		unsigned long long iVplan;
		unsigned long long x;
		if(thid<m){
			x=pivotes[thid];
			if ((x<=thid && x<n && fabs(M[thid+x*m])>=cTolerancia)||(x==thid)){
				iMplan=pivotes[m+thid];
				if(iMplan%2==0){//si el indice era par entonces agrego la rotacion al Vplan
					iMplan--; //me paro en el par que etsa guardado
					iVplan=atomicAdd(&Vplan[0], 1);//Obtengo un indice en el Vplan
					Vplan[iVplan*3+1]=x; //defino columna
					Vplan[iVplan*3+2]=thid; //defino fila1 (la que corresponde a este thread)
					Vplan[iVplan*3+3]=Mplan[x+iMplan*n]; //defino fila2 (la que guardo otro threand en el Mplan)
				}
			}
		}
	}

        __global__ void KernelResetCount(unsigned long long m, unsigned long long * Vplan,unsigned long long * Mplan,unsigned long long * pivotes){

                unsigned long long thid = threadIdx.x + blockDim.x * blockIdx.x;
		if (thid <m){
			Mplan[thid]=0;
			//pivotes[thid]=0;
			if (thid==0){
				Vplan[0]=0;
			}
		}

	}

	void GivensGPU2(float * resH, unsigned long long m, unsigned long long n,int indiceCantidadHilos,bool Debug){
		size_t TamanoDeMatriz = sizeof(float)*m*n;
		size_t TamanoDeVectorPlan = sizeof(unsigned long long)*(m*2+1);
		size_t TamanoDeMatrizPlan = sizeof(unsigned long long)*(n)*(m+1);
		size_t TamanoDePivotes = sizeof(unsigned long long)*m*2;

		float * resD;//Matriz resultado
		float * tempD;//Matriz auxiliar para las rotaciones
		unsigned long long * MplanD;//Matriz auxiliar para la planificacion
		unsigned long long * VplanD;//planificacion: lista con posiciones en la columna con valor distinto de cero. Priemer pos es el largo.  
		unsigned long long * pivotesD;  
		unsigned long long * VplanH = (unsigned long long*)malloc(TamanoDeVectorPlan);
		unsigned long long * MplanH = (unsigned long long*)malloc(TamanoDeMatrizPlan);
		unsigned long long * pivotesH = (unsigned long long*)malloc(TamanoDePivotes);
		bool bVerifica=false;
		VplanH[0]=0;

		// Allocate en device 
		CUDA_CALL(cudaMalloc(&resD, TamanoDeMatriz));
		CUDA_CALL(cudaMalloc(&tempD, TamanoDeMatriz));
		CUDA_CALL(cudaMalloc(&VplanD, TamanoDeVectorPlan));
		CUDA_CALL(cudaMalloc(&MplanD, TamanoDeMatrizPlan));
		CUDA_CALL(cudaMalloc(&pivotesD, TamanoDePivotes));
		
		// Inicializo matrices en el device
		CUDA_CALL(cudaMemcpy(resD, resH, TamanoDeMatriz, cudaMemcpyHostToDevice));
		CUDA_CALL(cudaMemset(pivotesD,0,TamanoDePivotes));
		CUDA_CALL(cudaMemset(MplanD,0,TamanoDeMatrizPlan));

		dim3 tamGrid(m/2);//Un bloque cada dos filas
		dim3 tamGrid2(m/CantidadHilos[indiceCantidadHilos] + (m%CantidadHilos[indiceCantidadHilos]==0?0:1));//un thread por fila
		dim3 tamBlock(CantidadHilos[indiceCantidadHilos]);

		do {
			if (VplanH[0]==0){
				bVerifica=true;//Recorro una vez mas para ver si las ultimas rotaciones cumplen la tolerancia
				CUDA_CALL(cudaMemset(pivotesD,0,TamanoDePivotes));
			}
			KernelResetCount<<<tamGrid2, tamBlock>>>(m,VplanD,MplanD,pivotesD);
			CUDA_CALL(cudaThreadSynchronize());
			KernelPlan2_M<<<tamGrid2, tamBlock>>>(m,n,resD,MplanD,pivotesD);
			CUDA_CALL(cudaThreadSynchronize());
			KernelPlan2_V<<<tamGrid2, tamBlock>>>(m,n,resD,VplanD,MplanD,pivotesD);
			CUDA_CALL(cudaThreadSynchronize());
			CUDA_CALL(cudaMemcpy(VplanH, VplanD, sizeof(unsigned long long), cudaMemcpyDeviceToHost));
			CUDA_CALL(cudaThreadSynchronize());
			if (VplanH[0] > 0){
				bVerifica=false;
				KernelGivens2<<<tamGrid, tamBlock>>>(m,n,resD,tempD,VplanD);
				CUDA_CALL(cudaThreadSynchronize());
				if(Debug){ 
					CUDA_CALL(cudaMemcpy(VplanH, VplanD, TamanoDeVectorPlan, cudaMemcpyDeviceToHost));
					CUDA_CALL(cudaThreadSynchronize());
					ImprimirPlan2(fp,VplanH);
					CUDA_CALL(cudaMemcpy(MplanH, MplanD,TamanoDeMatrizPlan, cudaMemcpyDeviceToHost));
					CUDA_CALL(cudaThreadSynchronize());
					ImprimirMatrizPlan(fp,MplanH, n);
					CUDA_CALL(cudaMemcpy(pivotesH, pivotesD, TamanoDePivotes, cudaMemcpyDeviceToHost));
					CUDA_CALL(cudaThreadSynchronize());
					ImprimirPivotes(fp,pivotesH,m);
					CUDA_CALL(cudaMemcpy(resH, resD, TamanoDeMatriz, cudaMemcpyDeviceToHost));
					CUDA_CALL(cudaThreadSynchronize());
					ImprimirMatriz(fp,resH,m,n);
				}
			}
		}while (VplanH[0] > 0 || !bVerifica);
		
		// Traer resultado;
		CUDA_CALL(cudaMemcpy(resH, resD, TamanoDeMatriz, cudaMemcpyDeviceToHost));
		CUDA_CALL(cudaThreadSynchronize());

		free(VplanH);
		free(MplanH);
		free(pivotesH);
		// Free matrices en device
		cudaFree(resD); 
		cudaFree(tempD); 
		cudaFree(VplanD); 
		cudaFree(MplanD); 
		cudaFree(pivotesD); 
	}

	__global__ void KernelReflexionHouseholderVariasOperaciones(unsigned long long m,float* refeD,float* normD,float* auxmD){
		unsigned long long x = blockIdx.x * blockDim.x + threadIdx.x;
		unsigned long long y = blockIdx.y * blockDim.y + threadIdx.y; 
		//OBJETIVO,RESOLVER ESTAS CUENTAS
		//v=u/norm(u);
		//ref=i-(2*v*v');
		//cada thread se encara de una celda del auxmD
		if (x<m && y<m){
			if (x==y){
				auxmD[y+x*m]=1-2*refeD[x]/normD[0]*refeD[y]/normD[0];
			}else{
				auxmD[y+x*m]=-2*refeD[x]/normD[0]*refeD[y]/normD[0];
			}
		}		
        }

	__global__ void KernelActualizacionParcialHouseholder(unsigned long long m, unsigned long long n,unsigned long long s,float* refeD,float* sub1D, float* sub2D, float* resuD){
		unsigned long long x = blockIdx.x * blockDim.x + threadIdx.x;
		unsigned long long y = blockIdx.y * blockDim.y + threadIdx.y; 
		unsigned long long dimX = n-s-1; 
		unsigned long long dimY = m-s-1; 
		//actualizo un elemento de la fila que no varia mas en el resultado final
		if (x>=s && x<n && y==s){
			resuD[s+x*m]=refeD[(x-s)*(m-s)];
		}
		//actualizo un elemento de la col que no varia mas en el resultado final
		if (y>=s && y<m && x==s){
			resuD[y+s*m]=refeD[y-s];
		}
		//Si es la ultima iteracion tambien copio la parte de adentro tb
		if ((n==s+1 || m==s+2) && x>=s && x<n && y>=s &&  y<m){
			resuD[y+x*m]=refeD[y-s+(x-s)*(m-s)];
		}
		//actualizo la matriz sub1 y sub2 con la sum matriz que luego hay que aplicarle householder
		if (x<dimX && y<dimY){
			sub1D[y+x*dimY]=refeD[(y+1)+(x+1)*(m-s)];
			sub2D[y+x*dimY]=refeD[(y+1)+(x+1)*(m-s)];
		}
	}

	void HouseholderGPU(float * resH,unsigned long long m, unsigned long long n,int indiceCantidadHilos,bool Debug){
		size_t TamanoDeMatrizO = sizeof(float)*m*n;
		size_t TamanoDeMatrizC = sizeof(float)*m*m;
		size_t TamanoDeVector = sizeof(float)*m;
		size_t TamanoDeFloat = sizeof(float);

		float * resuD;//Matriz resultado
		float * sub1D;//Matriz auxiliar
		float * sub2D;//Matriz auxiliar
		float * auxmD;//Matriz auxiliar
		float * refeD;//Matriz auxiliar
		float * normD;//Matriz auxiliar
                float normH;
                float * sub1H = (float*)malloc(sizeof(float)*m*n);

		// Allocate en device 
		CUDA_CALL(cudaMalloc(&resuD, TamanoDeMatrizO));
		CUDA_CALL(cudaMalloc(&refeD, TamanoDeMatrizO));
		CUDA_CALL(cudaMalloc(&sub1D, TamanoDeMatrizO));
		CUDA_CALL(cudaMalloc(&sub2D, TamanoDeMatrizO));
		CUDA_CALL(cudaMalloc(&auxmD, TamanoDeMatrizC));
		CUDA_CALL(cudaMalloc(&normD, TamanoDeFloat));
		
		// Inicializo matrices en el device
		CUDA_CALL(cudaMemcpy(resuD, resH, TamanoDeMatrizO, cudaMemcpyHostToDevice));
		CUDA_CALL(cudaMemcpy(sub1D, resH, TamanoDeMatrizO, cudaMemcpyHostToDevice));
		CUDA_CALL(cudaMemcpy(sub2D, resH, TamanoDeMatrizO, cudaMemcpyHostToDevice));
		CUDA_CALL(cudaMemcpy(refeD, resH, TamanoDeVector, cudaMemcpyHostToDevice));
		unsigned long long hilos = sqrt(CantidadHilos[indiceCantidadHilos]);
                unsigned long long cantBloquesY = m/hilos + ((m%hilos)==0?0:1);
                unsigned long long cantBloquesX = n/hilos + ((n%hilos)==0?0:1);
		dim3 tamGridM(cantBloquesX,cantBloquesY);//un thread por posicion
		dim3 tamGridC(cantBloquesY,cantBloquesY);//un thread por posicion
		dim3 tamBlock(hilos,hilos);

		for (unsigned long long col=0;col<(m>n?n:m);col++) {
			if (m-col>1){
                                normH=cublasSnrm2(m-col, sub1D, 1);
				CUDA_CALL(cudaThreadSynchronize());
				CUDA_CALL(cudaMemcpy(sub1H, sub1D, TamanoDeFloat, cudaMemcpyDeviceToHost));
				CUDA_CALL(cudaThreadSynchronize());
				normH = normH+sub1H[0];
				CUDA_CALL(cudaMemcpy(refeD, &normH, TamanoDeFloat, cudaMemcpyHostToDevice));
				CUDA_CALL(cudaThreadSynchronize());
                                normH=cublasSnrm2(m-col, refeD, 1);
				CUDA_CALL(cudaThreadSynchronize());
				CUDA_CALL(cudaMemcpy(normD, &normH, TamanoDeFloat, cudaMemcpyHostToDevice));
				CUDA_CALL(cudaThreadSynchronize());
	                	KernelReflexionHouseholderVariasOperaciones<<<tamGridC, tamBlock>>>(m-col,refeD,normD,auxmD);
				CUDA_CALL(cudaThreadSynchronize());
				cublasSgemm('N','N',m-col,n-col,m-col,1,auxmD,m-col,sub2D,m-col,0,refeD,m-col);
				CUDA_CALL(cudaThreadSynchronize());
				if(Debug){
				     	printf("normav[%f] vect0[%f]\n",normH,sub1H[0]);
				     	fprintf(fp,"normav[%f] vect0[%f]\n",normH,sub1H[0]);
				     	printf("matriz auxmD:\n");
				     	fprintf(fp,"matriz auxmD:\n");
	        			IprimirSubmatrizGPU(fp,auxmD,m-col,m-col,0,0,m-col,m-col);
				     	printf("matriz sub2D:\n");
				     	fprintf(fp,"matriz sub2D:\n");
	        			IprimirSubmatrizGPU(fp,sub2D,m-col,n-col,0,0,m-col,n-col);
				        printf("Resultado de la reflexion:\n");
				        fprintf(fp,"Resultado de la reflexion:\n");
	        			IprimirSubmatrizGPU(fp,refeD,m-col,n-col,0,0,m-col,n-col);
				}
                        	KernelActualizacionParcialHouseholder<<<tamGridM, tamBlock>>>(m,n,col,refeD,sub1D, sub2D, resuD); 
				CUDA_CALL(cudaThreadSynchronize());
		                //pongo la primer subcolumna al principio de refe
				CUDA_CALL(cudaMemcpy(refeD, sub1D, TamanoDeVector, cudaMemcpyDeviceToDevice));
				CUDA_CALL(cudaThreadSynchronize());
				if (Debug && n-col>1){
					CUDA_CALL(cudaMemcpy(sub1H, sub1D, TamanoDeMatrizO, cudaMemcpyDeviceToHost));
					CUDA_CALL(cudaThreadSynchronize());
			     		fprintf(fp,"Nueva submatriz sin la columna [%d]:\n",col);
		             		printf("Nueva submatriz sin la columna [%d]:\n",col);
					ImprimirMatriz(fp,sub1H, m-col-1,n-col-1);
                                        CUDA_CALL(cudaMemcpy(resH, resuD, TamanoDeMatrizO, cudaMemcpyDeviceToHost));
                                        CUDA_CALL(cudaThreadSynchronize());
                                        fprintf(fp,"Resultado global parcial:\n");
                                        printf("Resultado global parcial:\n");
                                        ImprimirMatriz(fp,resH, m,n);
				}
			}
		}
		
		// Traer resultado;
		CUDA_CALL(cudaMemcpy(resH, resuD, TamanoDeMatrizO, cudaMemcpyDeviceToHost));
		CUDA_CALL(cudaThreadSynchronize());
		if (Debug){
			fprintf(fp,"Matriz final de Householder\n");
		        printf("Matriz final de Householder\n");
	        	ImprimirMatriz(fp,resH,m,n);
		}
		free(sub1H);
		// Free matrices en device
		cudaFree(resuD); 
		cudaFree(refeD); 
		cudaFree(sub1D); 
		cudaFree(sub2D); 
		cudaFree(auxmD); 
		cudaFree(normD); 
	}

	void TestRendimiento(){

		bool bVerifica;
		float * matriz = (float*)malloc(sizeof(float)*3072*3072);;
		float * matrizO = (float*)malloc(sizeof(float)*3072*3072);;
                CargarMatrizAleatoria(matrizO,3072,3072,0,ProbabilidadCero,cSemilla);
		int valores[]={4, 8, 16, 32, 64, 128, 256, 512, 768, 1024, 2048, 3072};
		int n;
		for (int i=0; i<=11; i++){
			n=valores[i];
			if (n<=1024){
				memcpy(matriz,matrizO,sizeof(float)*n*n);
                                clockStart();
                		GivensCPU(fp,matriz, NULL,n,n,n,false);
                                duracion=clockStop();
				bVerifica = EsDiagonalSuperior(fp,matriz,n,n,cTolerancia);
				ImprimirResumen(fp,bPrimerLog, duracion, cFun_GivensCPU, n, n, 0, 0, 0, ProbabilidadCero,bVerifica);
			}

			if (n<=768){
				memcpy(matriz,matrizO,sizeof(float)*n*n);
                                clockStart();
				HouseholderCPU(fp,matriz,n,n,NULL,false);
		                duracion=clockStop();
				bVerifica = EsDiagonalSuperior(fp,matriz,n,n,cTolerancia);
				ImprimirResumen(fp,bPrimerLog,duracion, cFun_HouseholderCPU, n, n, 0, 0, 0, ProbabilidadCero,bVerifica);
			}

			memcpy(matriz,matrizO,sizeof(float)*n*n);
                	clockStart();
			HessenbergCPU(matriz,n,n,false,fp);
			duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,n,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, cFun_HessenbergCPU, n, n, 0, 0, 0, ProbabilidadCero,bVerifica);

			memcpy(matriz,matrizO,sizeof(float)*n*n);
                	clockStart();
                        GivensGPU1(matriz,n,n,cDefaultIndiceCantidadHilosGivens,false);
			duracion=clockStop();				bVerifica = EsDiagonalSuperior(fp,matriz,n,n,cTolerancia);
			bVerifica = EsDiagonalSuperior(fp,matriz,n,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, cFun_GivensGPU_1, n, n, 0, 0, CantidadHilos[cDefaultIndiceCantidadHilosGivens], ProbabilidadCero,bVerifica);
			
			memcpy(matriz,matrizO,sizeof(float)*n*n);
                	clockStart();
                	GivensGPU2(matriz,n,n,cDefaultIndiceCantidadHilosGivens,false);
			duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,n,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, cFun_GivensGPU_2, n, n, 0, 0, CantidadHilos[cDefaultIndiceCantidadHilosGivens], ProbabilidadCero,bVerifica);

			memcpy(matriz,matrizO,sizeof(float)*n*n);
                	clockStart();
                	HouseholderGPU(matriz,n,n,cDefaultIndiceCantidadHilosHouseholder,false);
			duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,n,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, cFun_HouseholderGPU, n, n, 0, 0, CantidadHilos[cDefaultIndiceCantidadHilosHouseholder], ProbabilidadCero,bVerifica);

			if (n<=1024){
				memcpy(matriz,matrizO,sizeof(float)*n*n);
                		clockStart();
                		GivensHibrido(matriz,n,n,cDimBloqueGivens,cDefaultIndiceCantidadHilosGivens,false);
				duracion=clockStop();
				bVerifica = EsDiagonalSuperior(fp,matriz,n,n,cTolerancia);
				ImprimirResumen(fp,bPrimerLog,duracion, cFun_GivensHibrido, n, n, 0, cDimBloqueGivens, CantidadHilos[cDefaultIndiceCantidadHilosGivens], ProbabilidadCero,bVerifica);
			}
			memcpy(matriz,matrizO,sizeof(float)*n*n);
                	clockStart();
                	HouseholderHibrido(matriz,n,n,cDimBloqueHouseholder,cDefaultIndiceCantidadHilosHouseholder,false);
			duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,n,n,1);
			ImprimirResumen(fp,bPrimerLog,duracion, cFun_HouseholderHibrido, n, n, 0, cDimBloqueHouseholder, CantidadHilos[cDefaultIndiceCantidadHilosHouseholder], ProbabilidadCero,bVerifica);
		}
		free (matriz);
		free (matrizO);
	}

        void TestHilos(){

                bool bVerifica;
		unsigned long long n=2048;
                float * matriz = (float*)malloc(sizeof(float)*n*n);;
                float * matrizO = (float*)malloc(sizeof(float)*n*n);;
               	CargarMatrizAleatoria(matrizO,n,n,0,ProbabilidadCero,cSemilla);
		int valores[]={512, 1024, 2048};				
		for (int i=0; i<=2; i++){
			n=valores[i];
                	for (int i=1; i<9; i++){
                	        memcpy(matriz,matrizO,sizeof(float)*n*n);
                	        clockStart();
                		GivensGPU2(matriz,n,n,i,false);
                	        duracion=clockStop();
                	        bVerifica = EsDiagonalSuperior(fp,matriz,n,n,cTolerancia);
                	        ImprimirResumen(fp,bPrimerLog,duracion, cFun_GivensGPU_2, n, n, 0, 0, CantidadHilos[i], ProbabilidadCero,bVerifica);
			}
                	for (int i=1; i<9; i++){
                	        memcpy(matriz,matrizO,sizeof(float)*n*n);
                	        clockStart();
                		HouseholderGPU(matriz,n,n,i,false);
                	        duracion=clockStop();
                	        bVerifica = EsDiagonalSuperior(fp,matriz,n,n,cTolerancia);
                	        ImprimirResumen(fp,bPrimerLog,duracion, cFun_HouseholderGPU, n, n, 0, 0, CantidadHilos[i], ProbabilidadCero,bVerifica);
                	}
		}
              	free (matrizO);
              	free (matriz);
        }

        void TestMatrices(){
                bool bVerifica;
                int maxDim=512;
                float * matriz = (float*)malloc(sizeof(float)*maxDim*maxDim);
                float * matrizO = (float*)malloc(sizeof(float)*maxDim*maxDim);
                unsigned long long m, n, hess;

                for (int M=1; M<=6; M++){
                        switch(M){
                                case 1:m=128;n=2048;hess=0;break;
                                case 2:m=128;n=2048;hess=96;break;
                                case 3:m=512;n=512;hess=0;break;
                                case 4:m=512;n=512;hess=384;break;
                                case 5:m=2048;n=128;hess=0;break;
                                case 6:m=2048;n=128;hess=1536;break;
			}
                	CargarMatrizAleatoria(matrizO,m,n,hess,ProbabilidadCero,cSemilla);

                        memcpy(matriz,matrizO,sizeof(float)*m*n);
                        clockStart();
                        GivensCPU(fp,matriz,NULL,m,n,m,false);
                        duracion=clockStop();
                        bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
                        ImprimirResumen(fp,bPrimerLog,duracion, cFun_GivensCPU, m, n, hess, 0, 0, ProbabilidadCero,bVerifica);

                        memcpy(matriz,matrizO,sizeof(float)*m*n);
                        clockStart();
                        HouseholderCPU(fp,matriz,m,n,NULL,false);
                        duracion=clockStop();
                        bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
                        ImprimirResumen(fp,bPrimerLog,duracion, cFun_HouseholderCPU, m, n, hess, 0, 0, ProbabilidadCero,bVerifica);

                        memcpy(matriz,matrizO,sizeof(float)*m*n);
                        clockStart();
                        GivensGPU2(matriz,m,n,cDefaultIndiceCantidadHilosGivens,false);
                        duracion=clockStop();
                        bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
                        ImprimirResumen(fp,bPrimerLog,duracion, cFun_GivensGPU_2, m, n, hess, 0, CantidadHilos[cDefaultIndiceCantidadHilosGivens], ProbabilidadCero,bVerifica);

                        memcpy(matriz,matrizO,sizeof(float)*m*n);
                        clockStart();
                        HouseholderGPU(matriz,m,n,cDefaultIndiceCantidadHilosHouseholder,false);
                        duracion=clockStop();
                        bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
                        ImprimirResumen(fp,bPrimerLog,duracion, cFun_HouseholderGPU, m, n, hess, 0, CantidadHilos[cDefaultIndiceCantidadHilosHouseholder], ProbabilidadCero,bVerifica);

                        memcpy(matriz,matrizO,sizeof(float)*m*n);
                        clockStart();
			GivensHibrido(matriz, m, n, cDimBloqueGivens, cDefaultIndiceCantidadHilosGivens,false);
                        duracion=clockStop();
                        bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
                        ImprimirResumen(fp,bPrimerLog,duracion, cFun_GivensHibrido, m, n, hess, cDimBloqueGivens, CantidadHilos[cDefaultIndiceCantidadHilosGivens], ProbabilidadCero,bVerifica);
                        memcpy(matriz,matrizO,sizeof(float)*m*n);
                        clockStart();
			HouseholderHibrido(matriz, m, n, cDimBloqueHouseholder, cDefaultIndiceCantidadHilosHouseholder,false);
                        duracion=clockStop();
                        bVerifica = EsDiagonalSuperior(fp,matriz,m,n,1);
                        ImprimirResumen(fp,bPrimerLog,duracion, cFun_HouseholderHibrido, m, n, hess, cDimBloqueHouseholder, CantidadHilos[cDefaultIndiceCantidadHilosHouseholder], ProbabilidadCero,bVerifica);
                }
		free(matriz);
		free(matrizO);
        }

        void TestBloques(){
                bool bVerifica;
                unsigned long long m=256;
                unsigned long long n=256;
                float * matriz = (float*)malloc(sizeof(float)*m*n);;
                float * matrizO = (float*)malloc(sizeof(float)*m*n);;

                CargarMatrizAleatoria(matrizO,m,n,0,ProbabilidadCero,cSemilla);

                for (int dimBloques=2; dimBloques<=16; dimBloques=dimBloques*2){
                   for (int i=1; i<=3; i++){
                        switch(i){
                                case 1:m=32;n=2048;break;
                                case 2:m=2048;n=32;break;
                                case 3:m=256;n=256;break;
			}
                        memcpy(matriz,matrizO,sizeof(float)*m*n);
                        clockStart();
			GivensHibrido(matriz,m,n,dimBloques,cDefaultIndiceCantidadHilosGivens,false);
                        duracion=clockStop();
                        bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
                        ImprimirResumen(fp,bPrimerLog,duracion, cFun_GivensHibrido, m, n, 0, dimBloques, 0, ProbabilidadCero,bVerifica);
                      
			memcpy(matriz,matrizO,sizeof(float)*m*n);
                        clockStart();
			HouseholderHibrido(matriz, m, n,dimBloques,cDefaultIndiceCantidadHilosHouseholder,false);
                        duracion=clockStop();
                        bVerifica = EsDiagonalSuperior(fp,matriz,m,n,1);
                        ImprimirResumen(fp,bPrimerLog,duracion, cFun_HouseholderHibrido, m, n, 0, dimBloques, 0, ProbabilidadCero,bVerifica);
		   }
		}
	}

	int main(int argc, char** argv){
		unsigned long long m =0;
		unsigned long long n =0;
		unsigned long long hess =0;
		char nombreArchivo[20];
		bool Debug = false;
		float * matriz;
		bool bVerifica=false;
                bool bError=false;

		if (argc < 2){
			printf("\nTransformaciones ortogonales de matices utilizando GPUs\n\n");
			printf("Ejecute nuevamente el programa indicando como parametro la simulacion o test desea correr\n\n");
			printf("%s - Simulacion de Givens en CPU *\n",cFun_GivensCPU);
			printf("%s - Simulacion de Givens en GPU version 1 *\n",cFun_GivensGPU_1);
			printf("%s - Simulacion de Givens en GPU version 2 *\n",cFun_GivensGPU_2);
			printf("%s - Simulacion de Givens Hibrido *\n",cFun_GivensHibrido);
			printf("%s - Simulacion de Householder en CPU *\n",cFun_HouseholderCPU);
			printf("%s - Simulacion de Householder en GPU *\n",cFun_HouseholderGPU);
			printf("%s - Simulacion de Householder Hibrido *\n",cFun_HouseholderHibrido);
			printf("%s - Simulacion de Hessenberg en CPU *\n",cFun_HessenbergCPU);
			printf("%s - Test de rendimiento\n",cFun_TestRendimiento);
			printf("%s - Test de matrices\n",cFun_TestMatrices);
			printf("%s - Test de hilos\n",cFun_TestHilos);
			printf("%s - Test de bloques\n",cFun_TestBloques);
			printf("\n* Para ejecutar las simulaciones se debe indicar:\n");
			printf("   2do Parametro: Cantidad de filas (entre 2 y 10000)\n");
			printf("   3do Parametro: Cantidad de columnas (entre 2 y 10000)\n");
			printf("   4to Parametro opcional: Para generar matriz inicial con forma de Hessenberg indicar alto del triangulo con ceros\n");
			printf("   5to Parametro opcional: Indicar una D para modo debug\n\n");
			exit(0);
		}

		char metodo[5];
		strcpy(metodo, argv[1]);
		sprintf(nombreArchivo,"Salida%s.txt",metodo);
		fp = fopen(nombreArchivo,"r+");
		if (!fp){
			fp = fopen(nombreArchivo, "w");
		}
		
		if (strcmp(metodo,cFun_GivensCPU)==0          || strcmp(metodo,cFun_GivensGPU_1)==0    || strcmp(metodo,cFun_GivensGPU_2)==0 || 
                    strcmp(metodo,cFun_GivensHibrido)==0      || strcmp(metodo,cFun_HouseholderCPU)==0 || strcmp(metodo,cFun_HouseholderGPU)==0 || 
                    strcmp(metodo,cFun_HouseholderHibrido)==0 || strcmp(metodo,cFun_HessenbergCPU)==0){ 
			if (argc>3){
				float mf=atof(argv[2]);
				float nf=atof(argv[3]);
				if (nf<1 || nf>10000 || mf<1 || nf>10000){
					printf("\nError, la dimensiones deben ser entre 2 y 10000.\n");
					fprintf(fp,"\nError, la dimensiones deben ser entre 2 y 10000.\n");
                                        bError=true;
				}
				m = (unsigned long long)mf;
				n = (unsigned long long)nf;
			        if (argc>4 && !bError){
				        float hessf=atof(argv[4]);
				        hess = (unsigned long long)hessf;
				        if (hess>=m){
					     printf("\nError, el segundo parametro [%llu] tiene que ser mayor que el cuarto paramentro [%llu]\n",m,hess);
					     fprintf(fp,"\nError, el segundo parametro [%llu] tiene que ser mayor que el cuarto paramentro [%llu]\n",m,hess);
                                             bError=true;
                                        }
			        } 
                                if (strcmp(argv[argc-1],"D")==0 && !bError){
				        Debug=true;
				        printf("\nModo debugo habilitado\n");
				        fprintf(fp,"\nModo debugo habilitado\n");
			        }	
		
                                if (!bError){
                                        matriz = (float*)malloc(sizeof(float)*m*n);
                                        CargarMatrizAleatoria(matriz,m,n,hess,ProbabilidadCero,cSemilla);
                	                if (Debug){
                        	                ImprimirMatriz(fp,matriz,m,n);
			                }
		                }
			}else{
				printf("\nEl alogoritmo indicado requiren dos parametros con las dimensiones de la matriz (entre 2 y 10000)\n");
                                bError=true;
			}
		}

                if (!bError)
		if (strcmp(cFun_GivensCPU,metodo)==0){
                        clockStart();
			GivensCPU(fp,matriz, NULL, m,n,m,Debug);
                        duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, metodo, m, n, hess, 0, 0, ProbabilidadCero,bVerifica);
			free(matriz);
		} else if (strcmp(cFun_GivensGPU_1,metodo)==0){
                        clockStart();
			GivensGPU1(matriz,m,n,cDefaultIndiceCantidadHilosGivens,Debug);
                        duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, metodo, m, n, hess, 0, CantidadHilos[cDefaultIndiceCantidadHilosGivens], ProbabilidadCero,bVerifica);
			free(matriz);
		} else if (strcmp(cFun_GivensGPU_2,metodo)==0){
                        clockStart();
			GivensGPU2(matriz,m,n,cDefaultIndiceCantidadHilosGivens,Debug);
                        duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, metodo, m, n, hess, 0, CantidadHilos[cDefaultIndiceCantidadHilosGivens], ProbabilidadCero,bVerifica);
			free(matriz);
		} else if (strcmp(cFun_GivensHibrido,metodo)==0){
                        clockStart();
			GivensHibrido(matriz,m,n,cDimBloqueGivens,cDefaultIndiceCantidadHilosGivens,Debug);
                        duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, metodo, m, n, hess, cDimBloqueGivens, CantidadHilos[cDefaultIndiceCantidadHilosGivens], ProbabilidadCero,bVerifica);
			free(matriz);
		} else if (strcmp(cFun_HouseholderCPU,metodo)==0){
                        clockStart();
			HouseholderCPU(fp,matriz,m,n,NULL,Debug);
		        duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, metodo, m, n, hess, 0, 0, ProbabilidadCero,bVerifica);
			free(matriz);
		} else if (strcmp(cFun_HouseholderGPU,metodo)==0){
                        clockStart();
			HouseholderGPU(matriz,m,n,cDefaultIndiceCantidadHilosHouseholder,Debug);
                        duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, metodo, m, n, hess, 0, CantidadHilos[cDefaultIndiceCantidadHilosHouseholder],ProbabilidadCero,bVerifica);
			free(matriz);
		} else if (strcmp(cFun_HouseholderHibrido,metodo)==0){
                        clockStart();
			HouseholderHibrido(matriz, m, n, cDimBloqueHouseholder,cDefaultIndiceCantidadHilosHouseholder,Debug);
                        duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,m,n,1);
			ImprimirResumen(fp,bPrimerLog,duracion, metodo, m, n, hess, cDimBloqueHouseholder,CantidadHilos[cDefaultIndiceCantidadHilosHouseholder], ProbabilidadCero,bVerifica);
			free(matriz);
		} else if (strcmp(cFun_HessenbergCPU,metodo)==0){
                        clockStart();
			HessenbergCPU(matriz, m, n, Debug,fp);
                        duracion=clockStop();
			bVerifica = EsDiagonalSuperior(fp,matriz,m,n,cTolerancia);
			ImprimirResumen(fp,bPrimerLog,duracion, metodo, m, n, hess, 0, 0, ProbabilidadCero,bVerifica);
			free(matriz);
		} else if (strcmp(cFun_TestHilos,metodo)==0){
			printf("Test de hilos\n",n);
			fprintf(fp,"Test de hilos\n",n);
			TestHilos();
		} else if (strcmp(cFun_TestRendimiento,metodo)==0){
			printf("Test de rendimiento\n",n);
			fprintf(fp,"Test de rendimiento\n");
			TestRendimiento();
		} else if (strcmp(cFun_TestMatrices,metodo)==0){
			printf("Test de matrices\n",n);
			fprintf(fp,"Test de matrices\n",n);
			TestMatrices();
		} else if (strcmp(cFun_TestBloques,metodo)==0){
			printf("Test de bloques\n",n);
			fprintf(fp,"Test de bloques\n",n);
			TestBloques();
		}else{
			printf("Metodo no implementado o invalido\n\n");
			exit(0);
                }
		
		printf("Los resultados fueron guardados en el archivo %s\n",nombreArchivo);	
		fclose (fp);

		return 0;
	}


